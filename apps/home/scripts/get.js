const axios = require('axios');
const pretty = require('pretty');

axios.get(process.argv[2])
  .then(res => {
    console.log(`${res.status} ${res.statusText}\n`);
    console.log(pretty(res.data));
  })
  .catch(err => {
    console.log('Error: ', err.message);
  });
