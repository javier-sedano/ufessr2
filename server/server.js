import path from "path";
import morgan from "morgan";
import { app } from "./app.js";
import "./static.js";
import "./index-html.js";

app.use(morgan("tiny"));
app.set("view engine", "ejs");
app.set("views", path.resolve(process.cwd(), "./server/views"));

const port = process.env.PORT || 9000;
console.log(`Listening at http://localhost:${port}/`);
app.listen(port);
