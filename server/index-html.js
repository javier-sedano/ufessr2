import fetch from "node-fetch";
import { app } from "./app.js";
import {
  constructServerLayout,
  sendLayoutHTTPResponse,
} from "single-spa-layout/server";
import _ from "lodash";

const urls = {
  "@jsedano-ufessr/home": "http://localhost:3010",
};

async function fetchMicrofrontend(props) {
  const url = urls[props.name];
  console.log(`  Fetching ${url} ...`);
  return fetch(`${url}`, {
    headers: props,
  }).then((res) => {
    console.log(`  ${url} --> ${res.status} ${res.statusText}`);
    if (res.ok) {
      return res;
    } else {
      throw Error(
        `Received http response ${res.status} from microfrontend ${props.name}`
      );
    }
  });
}

const serverLayout = constructServerLayout({
  filePath: "server/views/index.html",
});

app.use("*", (req, res, next) => {
  // const developmentMode = process.env.NODE_ENV === "development";
  // const importSuffix = developmentMode ? `?ts=${Date.now()}` : "";

  const props = {};
  const fragments = {};
  const fetchPromises = {};

  const renderFragment = (name) => fragments[name]();

  sendLayoutHTTPResponse({
    serverLayout,
    urlPath: req.originalUrl,
    res,
    renderFragment,
    async renderApplication({ appName, propsPromise }) {
      const props = await propsPromise;
      const fetchPromise =
        fetchPromises[appName] ||
        (fetchPromises[appName] = fetchMicrofrontend(props));
      const response = await fetchPromise;
      return response.body;
    },
    async retrieveApplicationHeaders({ appName, propsPromise }) {
      const props = await propsPromise;
      const fetchPromise =
        fetchPromises[appName] ||
        (fetchPromises[appName] = fetchMicrofrontend(props));
      const response = await fetchPromise;
      return response.headers;
    },
    async retrieveProp(propName) {
      return props[propName];
    },
    assembleFinalHeaders(allHeaders) {
      return Object.assign(
        {},
        ...Object.values(allHeaders).map((a) => a.appHeaders)
      );
    },
  })
    .then(next)
    .catch((err) => {
      console.error(err);
      res.status(500).send("A server error occurred");
    });
});
